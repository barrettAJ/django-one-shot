from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list(request):
    todo = TodoList.objects.all()
    context = {
        'todo_list': todo,
    }
    return render(request, 'todos/list.html', context)

def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    todo_items = todo.items.all()
    context = {
        'todo_list_detail': todo,
        'todo_items': todo_items,
    }
    return render(request, 'todos/detail.html', context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect('todo_list_detail', id=todo.id)
    else:
        form = TodoListForm()
    context = {
        'form': form
    }
    return render(request, 'todos/create.html', context)

def edit_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)

    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        'form': form
    }
    return render(request, 'todos/edit_list.html', context)

def delete_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todolist.delete()
        return redirect('todo_list_list')
    return render(request, 'todos/delete.html')

def create_todo_item(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=form.cleaned_data['list'].id)

    else:
        form = TodoItemForm()
    context = {
        'form':form
    }
    return render(request, 'todos/create.html', context)

def edit_todo_item(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=form.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context ={
        'form':form
    }
    return render(request, 'todos/edit_item.html', context)
